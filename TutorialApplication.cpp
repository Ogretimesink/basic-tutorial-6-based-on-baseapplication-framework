/*
-----------------------------------------------------------------------------
Filename:	TutorialApplication.cpp
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

// Required header for Apple systems
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#include <macUtils.h>
#endif

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: mRoot(0)
	, mCamera(0)
	, mSceneMgr(0)
	, mWindow(0)
	, mResourcesCfg(Ogre::StringUtil::BLANK)
	, mPluginsCfg(Ogre::StringUtil::BLANK)
	, mTrayMgr(0)
	, mCameraMan(0)
	, mDetailsPanel(0)
	, mCursorWasVisible(false)
	, mShutDown(false)
	, mInputManager(0)
	, mMouse(0)
	, mKeyboard(0)
	, mOverlaySystem(0)
{

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
	// Default resource path for Apple platform
	m_ResourcePath = Ogre::macBundlePath() + "/Contents/Resources/";
#else
	// Default resource path for all other platforms
	m_ResourcePath = "";
#endif
}

//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
	// Destroy the cursor, backdrop, tray, and widget manager object
	if (mTrayMgr) delete mTrayMgr;

	// Destroy the basic camera controller object
	if (mCameraMan) delete mCameraMan;

	// Destroy the overlay manager object
	if (mOverlaySystem) delete mOverlaySystem;

	// Remove the application as a Window listener
	Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);

	// Call function to unattach OIS before window shutdown
	windowClosed(mWindow);

	// Delete the root object instance for the application
	delete mRoot;
}
//---------------------------------------------------------------------------
void TutorialApplication::go(void)
{

#ifdef _DEBUG
#ifndef OGRE_STATIC_LIB
	// Set the debug build resource configuration files for static libraries
	mResourcesCfg = m_ResourcePath + "resources_d.cfg";

	// Set the debug build plugin configuration files for static libraries
	mPluginsCfg = m_ResourcePath + "plugins_d.cfg";
#else
	// Set the debug build resource configuration files for non-static libraries
	mResourcesCfg = "resources_d.cfg";

	// Set the debug build plugin configuration files for non-static libraries
	mPluginsCfg = "plugins_d.cfg";
#endif
#else
#ifndef OGRE_STATIC_LIB
	// Set the release build resource configuration files for static libraries
	mResourcesCfg = m_ResourcePath + "resources.cfg";

	// Set the release build plugin configuration files for static libraries
	mPluginsCfg = m_ResourcePath + "plugins.cfg";
#else
	// Set the release build resource configuration files for non-static libraries
	mResourcesCfg = "resources.cfg";

	// Set the release build plugin configuration files for non-static libraries
	mPluginsCfg = "plugins.cfg";
#endif
#endif

	// Call function to set up individual parts of the application
	if (!setup())
		return;

	// Start the automatic scene rendering cycle
	mRoot->startRendering();

	// Call function to clean up objects before shutdown
	destroyScene();
}
//---------------------------------------------------------------------------
bool TutorialApplication::setup(void)
{
	// Create root object instance for the application
	mRoot = new Ogre::Root(mPluginsCfg);

	// Call function to load resources from configuration files
	setupResources();

	// Call function to show the configuration dialog box
	bool carryOn = configure();

	// Check status returned
	if (!carryOn) return false;

	// Call function to initialize the scene manager and OverlaySystem objects 
	chooseSceneManager();

	// Call function to initialize the scene camera
	createCamera();

	// Call function to initialize the scene viewports
	createViewports();

	// Set default mipmap level (NB some APIs ignore this)
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

	// Call function to create any resource listeners for loading screens
	createResourceListener();

	// Call function to initialize all of the resources found by the application
	loadResources();

	// Call function to fill the scene with graphic objects
	createScene();

	// Call function to initialize input objects
	createFrameListener();

	return true;
};
//---------------------------------------------------------------------------
void TutorialApplication::setupResources(void)
{
	// Declare object for configuration file
	Ogre::ConfigFile cf;

	// Load settings data from configuration file
	cf.load(mResourcesCfg);

	// Declare an iterator to load sections of the resource configuration file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	// Declare strings into which to load configuration data
	Ogre::String secName, typeName, archName;

	// Iterate through the resource configuration file
	while (seci.hasMoreElements())
	{
		// Get the current element from the resource without advancing
		secName = seci.peekNextKey();

		// Declare multimap settings pair and load a section
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();

		// Declare iterator for multimap items in section
		Ogre::ConfigFile::SettingsMultiMap::iterator i;

		// Loop through multimap settings in the section
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			// Unpack the resource location type
			typeName = i->first;

			// Unpack the resource location path
			archName = i->second;

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
			// OS X does not set the working directory relative to the app.
			// In order to make things portable on OS X we need to provide
			// the loading with it's own bundle path location.
			if (!Ogre::StringUtil::startsWith(archName, "/", false)) // only adjust relative directories
				archName = Ogre::String(Ogre::macBundlePath() + "/" + archName);
#endif
			// Add the unpacked resources to the application 
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
		}
	}
}
//---------------------------------------------------------------------------
bool TutorialApplication::configure(void)
{
	// Show the configuration dialog and initialise the system.
	// You can skip this and use root.restoreConfig() to load configuration
	// settings if you were sure there are valid ones saved in ogre.cfg.
	if(mRoot->showConfigDialog())
	{
		// If returned true, user clicked OK so initialise.
		// Here we choose to let the system create a default rendering window by passing 'true'.
		mWindow = mRoot->initialise(true, "TutorialApplication Render Window");

		return true;
	}
	else
	{
		return false;
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::chooseSceneManager(void)
{
	// Initialize the scene manager object
	mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);

	// Initialize the OverlaySystem object (changed for Ogre 1.9)
	mOverlaySystem = new Ogre::OverlaySystem();

	// Register the OverlaySystem to be called when rendering queue is processed
	mSceneMgr->addRenderQueueListener(mOverlaySystem);
}
//---------------------------------------------------------------------------
void TutorialApplication::createCamera(void)
{
	// Initialize the scene camera
	mCamera = mSceneMgr->createCamera("PlayerCam");

	// Position the camera within the scene
	mCamera->setPosition(Ogre::Vector3(0,0,80));
	
	// Set a position to point the camera
	mCamera->lookAt(Ogre::Vector3(0,0,-300));

	// Set the distance the camera won't render meshes
	mCamera->setNearClipDistance(5);

	// Initialize the default camera controller
	mCameraMan = new OgreBites::SdkCameraMan(mCamera);
}
//---------------------------------------------------------------------------
void TutorialApplication::createViewports(void)
{
	// Declare and initialize a scene rendering viewpoint for the camera
	Ogre::Viewport* vp = mWindow->addViewport(mCamera);

	// Set the scene background color
	vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

	// Set the viewport size
	mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//---------------------------------------------------------------------------
void TutorialApplication::createResourceListener(void)
{
}
//---------------------------------------------------------------------------
void TutorialApplication::loadResources(void)
{
	// Initialize all of the resources found by the application
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create an instance of a computer graphic model character
	Ogre::Entity* ogreEntity = mSceneMgr->createEntity("ogrehead.mesh");
 
	// Create an object in the scene
	Ogre::SceneNode* ogreNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();

	// Add the entity to the object
	ogreNode->attachObject(ogreEntity);

	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(.5, .5, .5));
 
	// Create a directional light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight("MainLight");

	// Position the light
	light->setPosition(20, 80, 50);
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Write message to log about the start of the initialization of input devices
	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

	// Create a list of parameters
	OIS::ParamList pl;

	// Declare unsigned integer for the window handle
	size_t windowHnd = 0;

	// Declare stream to name for the window handle
	std::ostringstream windowHndStr;

	// Get the window handle
	mWindow->getCustomAttribute("WINDOW", &windowHnd);

	// Bitwise left shift?? 
	windowHndStr << windowHnd;

	// Pass the name and window handle as a pair to the list
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

	// Create the input device manager
	mInputManager = OIS::InputManager::createInputSystem(pl);

	// Create the keyboard input device object
	mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));

	// Create the mouse input device object
	mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));

	// Register as a mouse listener
	mMouse->setEventCallback(this);

	// Register as a keyboard listener
	mKeyboard->setEventCallback(this);

	// Set initial mouse clipping size
	windowResized(mWindow);

	// Register the window as a Window listener
	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

	// Make an OgreBites input context for the keyboard
	mInputContext.mKeyboard = mKeyboard;

	// Make an OgreBites input context for the mouse
	mInputContext.mMouse = mMouse;

	// Create a tray manager for input via the keyboard and mouse contexts
	mTrayMgr = new OgreBites::SdkTrayManager("InterfaceName", mWindow, mInputContext, this);

	// Show the frame stats in the bottom left
	mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);

	// Show the logo in the bottom right
	mTrayMgr->showLogo(OgreBites::TL_BOTTOMRIGHT);

	// Hide the cursor
	mTrayMgr->hideCursor();

	// Create a params panel for displaying sample details
	Ogre::StringVector items;

	// Zeroth position is the cameras X position
	items.push_back("cam.pX");

	// 1st position is the cameras Y position
	items.push_back("cam.pY");

	// 2nd position is the cameras Z position
	items.push_back("cam.pZ");

	// 3rd position is a filler
	items.push_back("");

	// 4th position is the camera orientation
	items.push_back("cam.oW");

	// 5th position is the camera orientation
	items.push_back("cam.oX");

	// 6th position is the camera orientation
	items.push_back("cam.oY");

	// 7th position is the camera orientation
	items.push_back("cam.oZ");

	// 8th position is a filler
	items.push_back("");

	// 9th position is the texture filter method which is gives varying image quality
	items.push_back("Filtering");

	// 10th position is the polygon mode
	items.push_back("Poly Mode");

	// Create the debugging details panel and our details in "items"
	mDetailsPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "DetailsPanel", 200, items);

	// Create a section in the 9th position with the original value as "Bilinear"
	mDetailsPanel->setParamValue(9, "Bilinear");

	// Create a section in the 10th position with the original value as "Bilinear"
	mDetailsPanel->setParamValue(10, "Solid");

	// Don't make the detail panel visible
	mDetailsPanel->hide();

	// Add this function createFrameListener as a frame listener
	mRoot->addFrameListener(this);
}
//---------------------------------------------------------------------------
void TutorialApplication::windowResized(Ogre::RenderWindow* rw)
{
	// Declare rendering area variables
	unsigned int width, height, depth;

	// Declare rendering area variables
	int left, top;

	// Get information about the rendering area
	rw->getMetrics(width, height, depth, left, top);

	// Retrieve the current state of the mouse
	const OIS::MouseState &ms = mMouse->getMouseState();

	// Set the mouse clipping area width to that of the display area
	ms.width = width;

	// Set the mouse clipping area height to that of the display area
	ms.height = height;
}
//---------------------------------------------------------------------------
void TutorialApplication::windowClosed(Ogre::RenderWindow* rw)
{
	// Check for valid application window
	if(rw == mWindow)
	{
		// Check for valid input device manager
		if(mInputManager)
		{
			// Destroy the mouse input device object
			mInputManager->destroyInputObject(mMouse);

			// Destroy the keyboard input device object
			mInputManager->destroyInputObject(mKeyboard);

			// Destroy the input device manager
			OIS::InputManager::destroyInputSystem(mInputManager);

			// Re-initialize the input device manager before application shutdown
			mInputManager = 0;
		}
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	// Check the windows status
	if(mWindow->isClosed())

		// End the application
		return false;

	// Check the application shutdown flag
	if(mShutDown)

		// End the application
		return false;

	// Check the keyboard input device
	mKeyboard->capture();

	// Check the mouse input device
	mMouse->capture();

	// Update the tray manager object
	mTrayMgr->frameRenderingQueued(evt);

	// Check the tray manager object visibility
	if (!mTrayMgr->isDialogVisible())
	{
		// Update the scene camera
		mCameraMan->frameRenderingQueued(evt);

		// Check the debugging details window visibility
		if (mDetailsPanel->isVisible())		  
		{
			// Update the debugging details window contents
			mDetailsPanel->setParamValue(0, Ogre::StringConverter::toString(mCamera->getDerivedPosition().x));
			mDetailsPanel->setParamValue(1, Ogre::StringConverter::toString(mCamera->getDerivedPosition().y));
			mDetailsPanel->setParamValue(2, Ogre::StringConverter::toString(mCamera->getDerivedPosition().z));
			mDetailsPanel->setParamValue(4, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().w));
			mDetailsPanel->setParamValue(5, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().x));
			mDetailsPanel->setParamValue(6, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().y));
			mDetailsPanel->setParamValue(7, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().z));
		}
	}

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed( const OIS::KeyEvent &arg )
{
	// don't process any more keys if dialog is visible
	if (mTrayMgr->isDialogVisible()) return true;

	// Check for the F key
	if (arg.key == OIS::KC_F)
	{
		// Toggle visibility of advanced frame rendering statistics
		mTrayMgr->toggleAdvancedFrameStats();
	}
	// Check for the G key
	else if (arg.key == OIS::KC_G)
	{
		// Check location of the debugging details window
		if (mDetailsPanel->getTrayLocation() == OgreBites::TL_NONE)
		{
			// Move the debugging details window to the top right location
			mTrayMgr->moveWidgetToTray(mDetailsPanel, OgreBites::TL_TOPRIGHT, 0);
						
			// Toggle visibility of debugging details window
			mDetailsPanel->show();
		}
		else
		{
			// Move the debugging details window off screen
			mTrayMgr->removeWidgetFromTray(mDetailsPanel);

			// Toggle visibility of debugging details window
			mDetailsPanel->hide();
		}
	}
	// Check for the T key
	else if (arg.key == OIS::KC_T)
	{
		// Declare string for texture filter method
		Ogre::String newVal;

		// Declare option for texture filter method
		Ogre::TextureFilterOptions tfo;

		// Declare anistrophy
		unsigned int aniso;

		// Retrieve the debugging details window texture filter method
		switch (mDetailsPanel->getParamValue(9).asUTF8()[0])
		{
			case 'B':
				// Set texture filter string to trilinear
				newVal = "Trilinear";

				// Set texture filter option to trilinear method
				tfo = Ogre::TFO_TRILINEAR;

				// Set the anistrophy value
				aniso = 1;
				break;
			case 'T':
				// Set texture filter string to anistropic
				newVal = "Anisotropic";

				// Set texture filter option to anistropic method
				tfo = Ogre::TFO_ANISOTROPIC;

				// Set the anistrophy value
				aniso = 8;
				break;
			case 'A':
				// Set texture filter string to no filtering
				newVal = "None";

				// Set texture filter option to no method
				tfo = Ogre::TFO_NONE;

				// Set the anistrophy value
				aniso = 1;
				break;
			default:
				// Set texture filter string to bilinear
				newVal = "Bilinear";

				// Set texture filter option to bilinear method
				tfo = Ogre::TFO_BILINEAR;

				// Set the anistrophy value
				aniso = 1;
		}

		// Set the new texture filter method
		Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(tfo);

		// Set the new anistrophy value
		Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(aniso);

		// Update the debugging details to the new window texture filter method
		mDetailsPanel->setParamValue(9, newVal);
	}
	// Check for the R key
	else if (arg.key == OIS::KC_R)
	{
		// Declare polygon mode string
		Ogre::String newVal;

		// Declare polygon mode
		Ogre::PolygonMode pm;

		// Retrieve the polygon mode
		switch (mCamera->getPolygonMode())
		{
		case Ogre::PM_SOLID:
			// Set the polygon mode string to wireframe
			newVal = "Wireframe";

			// Set the polygon mode to wireframe
			pm = Ogre::PM_WIREFRAME;
			break;
		case Ogre::PM_WIREFRAME:
			// Set the polygon mode string to points
			newVal = "Points";

			// Set the polygon mode to points
			pm = Ogre::PM_POINTS;
			break;
		default:
			// Set the polygon mode string to solid
			newVal = "Solid";

			// Set the polygon mode to solid
			pm = Ogre::PM_SOLID;
		}

		// Set the new polygon mode
		mCamera->setPolygonMode(pm);

		// Update the debugging detail window to the new polygon mode
		mDetailsPanel->setParamValue(10, newVal);
	}
	// Check for the F5 key
	else if(arg.key == OIS::KC_F5)
	{
		// Refresh all textures in the scene
		Ogre::TextureManager::getSingleton().reloadAll();
	}
	// Check for the print screen key
	else if (arg.key == OIS::KC_SYSRQ)
	{
		// Take a time stamped screenshot and save to same directory as application
		mWindow->writeContentsToTimestampedFile("screenshot", ".jpg");
	}
	// Check for the escape key
	else if (arg.key == OIS::KC_ESCAPE)
	{
		// Set the flag to shutdown the applicaion
		mShutDown = true;
	}

	// Send keyboard input to the OgreBites camera controller
	mCameraMan->injectKeyDown(arg);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent &arg)
{
	// Send keyboard input to the OgreBites camera controller
	mCameraMan->injectKeyUp(arg);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent &arg)
{
	// Send mouse input to the OgreBites tray manager
	if (mTrayMgr->injectMouseMove(arg))
		return true;

	// Send mouse input to the OgreBites camera controller
	mCameraMan->injectMouseMove(arg);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	// Send mouse input to the OgreBites tray manager
	if (mTrayMgr->injectMouseDown(arg, id))
		return true;

	// Send mouse input to the OgreBites camera controller
	mCameraMan->injectMouseDown(arg, id);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	// Send mouse input to the OgreBites tray manager
	if (mTrayMgr->injectMouseUp(arg, id))
		return true;

	// Send mouse input to the OgreBites camera controller
	mCameraMan->injectMouseUp(arg, id);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	// For windows playtform
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	// For linux and apple platform
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			// Display windows error message box
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			// Display error message to linux and apple standard output error stream
			std::cerr << "An exception has occurred: " << e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
