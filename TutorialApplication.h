/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#  include <OIS/OISEvents.h>
#  include <OIS/OISInputManager.h>
#  include <OIS/OISKeyboard.h>
#  include <OIS/OISMouse.h>

#  include <OGRE/SdkTrays.h>
#  include <OGRE/SdkCameraMan.h>
#else
#  include <OISEvents.h>
#  include <OISInputManager.h>
#  include <OISKeyboard.h>
#  include <OISMouse.h>

#  include <SdkTrays.h>
#  include <SdkCameraMan.h>
#endif

#ifdef OGRE_STATIC_LIB
#  define OGRE_STATIC_GL
#  if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#    define OGRE_STATIC_Direct3D9
// D3D10 will only work on vista, so be careful about statically linking
#    if OGRE_USE_D3D10
#      define OGRE_STATIC_Direct3D10
#    endif
#  endif
#  define OGRE_STATIC_BSPSceneManager
#  define OGRE_STATIC_ParticleFX
#  define OGRE_STATIC_CgProgramManager
#  ifdef OGRE_USE_PCZ
#    define OGRE_STATIC_PCZSceneManager
#    define OGRE_STATIC_OctreeZone
#  else
#    define OGRE_STATIC_OctreeSceneManager
#  endif
#  include "OgreStaticPluginLoader.h"
#endif

//---------------------------------------------------------------------------

class TutorialApplication : public Ogre::FrameListener, public Ogre::WindowEventListener, public OIS::KeyListener, public OIS::MouseListener, OgreBites::SdkTrayListener
{
public:
	// C++ application constructor
	TutorialApplication(void);

	// C++ application destructor
	virtual ~TutorialApplication(void);

	// Start the application
	virtual void go(void);

protected:
	// Function to set up individual parts of the application
	virtual bool setup();

	// Function to load resources from configuration files
	virtual void setupResources(void);

	// Function to show the configuration dialog box
	virtual bool configure(void);

	// Function to initialize the scene manager and OverlaySystem objects
	virtual void chooseSceneManager(void);

	// Function to initialize the scene camera
	virtual void createCamera(void);

	// Function to initialize the scene viewports
	virtual void createViewports(void);

	// Function to create any resource listeners for loading screens
	virtual void createResourceListener(void);

	// Function to initialize all of the resources found by the application
	virtual void loadResources(void);

	// Function to fill the scene with graphic objects
	virtual void createScene(void);

	// Function to initialize input objects
	virtual void createFrameListener(void);

	// Function to clean up objects before shutdown
	virtual void destroyScene(void);

	// Function to update scene every frame
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

	// Function to proces keyboard input
	virtual bool keyPressed(const OIS::KeyEvent &arg);

	// Function to proces keyboard input
	virtual bool keyReleased(const OIS::KeyEvent &arg);

	// Function to proces mouse input
	virtual bool mouseMoved(const OIS::MouseEvent &arg);

	// Function to proces mouse input
	virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	// Function to proces mouse input
	virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	// Adjust mouse clipping area
	virtual void windowResized(Ogre::RenderWindow* rw);

	// Unattach OIS before window shutdown
	virtual void windowClosed(Ogre::RenderWindow* rw);

	// Root object instance for the application
	Ogre::Root*					mRoot;

	// Camera object for viewing the scene
	Ogre::Camera*				mCamera;

	// Scene manager object that controls all things rendered on the screen
	Ogre::SceneManager*			mSceneMgr;

	// Window object instance for rendering the application
	Ogre::RenderWindow*			mWindow;

	// Resource configuration file name
	Ogre::String				mResourcesCfg;

	// Plugin configuration file name
	Ogre::String				mPluginsCfg;

	// Overlay manager object
	Ogre::OverlaySystem*		mOverlaySystem;

	// OgreBites
	OgreBites::InputContext		mInputContext;

	// Cursor, backdrop, tray, and widget manager object
	OgreBites::SdkTrayManager*	mTrayMgr;

	// Basic camera controller object
	OgreBites::SdkCameraMan*	mCameraMan;

	// Debugging details window
	OgreBites::ParamsPanel*		mDetailsPanel;

	// Was cursor visible before dialog appeared?
	bool						mCursorWasVisible;

	// Flag to shutdown the application
	bool						mShutDown;

	// OIS Input device manager object
	OIS::InputManager*			mInputManager;

	// Mouse input device object
	OIS::Mouse*					mMouse;

	// Keyboard input device object
	OIS::Keyboard*				mKeyboard;

	// Added resource path for Mac compatibility
	Ogre::String				m_ResourcePath;

#ifdef OGRE_STATIC_LIB
	// 
	Ogre::StaticPluginLoader m_StaticPluginLoader;
#endif
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------